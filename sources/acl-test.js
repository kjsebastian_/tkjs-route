/* global FIXTURE, TEST */
var NI = require('tkjs-ni');
var ACL = require('./acl');

FIXTURE("ACL", function() {

  TEST("checkUserACL_SuperAdmin", function() {
    var superAdminACL = { __superadmin__: true };
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, "foo"));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, "bar"));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, "narf"));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, "967CF003-75C0-432D-BFF5-307CA6AB382F"));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, ["foo","bar"]));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, ["foo","bar","narf"]));
  })

  TEST("checkUserACL_Test", function() {
    var testUserACL = { foo: true, bar: true };
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, "foo"));
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, "bar"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "narf"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "967CF003-75C0-432D-BFF5-307CA6AB382F"));
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, ["foo","bar"]));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, ["foo","bar","narf"]));
  })

  TEST("checkUserACL false values", function() {
    var testUserACL = { foo: true, bar: true, baz: false };
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, "foo"));
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, "bar"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "baz"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "narf"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "967CF003-75C0-432D-BFF5-307CA6AB382F"));

    NI.assert.isTrue(ACL.checkUserACL(testUserACL, ["foo","bar"]));
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, ["OR","baz","foo"]));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, ["AND","baz","foo"]));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, ["foo","bar","narf"]));
  })
})
