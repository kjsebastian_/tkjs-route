var NI = require('tkjs-ni');

function checkUserACL(aUserACL, aRequiredACL) {
  if (typeof aUserACL !== "object") {
    return false;
  }
  if (aUserACL.__superadmin__ === true) {
    return true;
  }

  if (NI.isEmpty(aRequiredACL)) {
    return !NI.isEmpty(aUserACL);
  }
  else if (NI.isArray(aRequiredACL)) {
    var i = 0;
    var len = NI.size(aRequiredACL);
    if (aRequiredACL[0] == "OR") {
      i = 1;
      for ( ; i < len; i += 1) {
        if (aUserACL[aRequiredACL[i]]) {
          return true;
        }
      }
      return false;
    }
    else {
      if (aRequiredACL[0] == "AND") {
        i = 1;
      }
      for ( ; i < len; i += 1) {
        if (!aUserACL[aRequiredACL[i]]) {
          return false;
        }
      }
      return true;
    }
  }
  else {
    return (aUserACL[aRequiredACL]);
  }
}
exports.checkUserACL = checkUserACL;

function checkIsUser(aUserNameOrEmail, aSession) {
  NI.assert.stringNotEmpty(aUserNameOrEmail);
  if (NI.isEmpty(aSession)) {
    return false;
  }

  aUserNameOrEmail = aUserNameOrEmail.toLowerCase();
  var sessionUserName = (aSession.user_name || "").toLowerCase();
  if (sessionUserName == aUserNameOrEmail) {
    return true;
  }

  var sessionUserEmail = (aSession.user_email || "").toLowerCase();
  if (sessionUserEmail == aUserNameOrEmail) {
    return true;
  }

  return false;
}
exports.checkIsUser = checkIsUser;
