var NI = require('tkjs-ni');
var crypto = require("crypto");

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var COOKIE_NAME_SEP = '=';

var KDF_ENC = 'encode_cookie-encryption-053F4B74';
var KDF_MAC = 'encode_cookie-signature-5C6ABB01';

var NUMS_BASE = 36;

exports.DEFAULT_ENCRYPTION_ALGO = 'aes256';
exports.DEFAULT_SIGNATURE_ALGO = 'sha256';

function base64urlencode(arg) {
  var s = arg.toString('base64');
  s = s.split('=')[0]; // Remove any trailing '='s
  s = s.replace(/\+/g, '-'); // 62nd char of encoding
  s = s.replace(/\//g, '_'); // 63rd char of encoding
  // TODO optimize this; we can do much better
  return s;
}

var base64urldecode = NI.b64decodeBuffer;

function forceBuffer(binaryOrBuffer) {
  if (Buffer.isBuffer(binaryOrBuffer)) {
    return binaryOrBuffer;
  } else {
    return new Buffer(binaryOrBuffer, 'binary');
  }
}

function deriveKey(master, type) {
  // eventually we want to use HKDF. For now we'll do something simpler.
  var hmac = crypto.createHmac('sha256', master);
  hmac.update(type);
  return forceBuffer(hmac.digest());
}

function setupKeys(opts) {
  NI.assert.has(opts,"cookieName");
  NI.assert.has(opts,"secret");

  // derive two keys, one for signing one for encrypting, from the secret.
  if (!opts.encryptionKey) {
    opts.encryptionKey = deriveKey(opts.secret, KDF_ENC);
  }

  if (!opts.signatureKey) {
    opts.signatureKey = deriveKey(opts.secret, KDF_MAC);
  }

  if (!opts.signatureAlgorithm) {
    opts.signatureAlgorithm = exports.DEFAULT_SIGNATURE_ALGO;
  }

  if (!opts.encryptionAlgorithm) {
    opts.encryptionAlgorithm = exports.DEFAULT_ENCRYPTION_ALGO;
  }
}
exports.setupKeys = setupKeys;

function constantTimeEquals(a, b) {
  // Ideally this would be a native function, so it's less sensitive to how the
  // JS engine might optimize.
  if (a.length !== b.length) {
    return false;
  }
  var ret = 0;
  for (var i = 0; i < a.length; i++) {
    ret |= a.readUInt8(i) ^ b.readUInt8(i);
  }
  return ret === 0;
}
exports.constantTimeEquals = constantTimeEquals;

// it's good cryptographic pracitice to not leave buffers with sensitive
// contents hanging around.
function zeroBuffer(buf) {
  for (var i = 0; i < buf.length; i++) {
    buf[i] = 0;
  }
  return buf;
}

function hmacInit(algo, key) {
  var match = algo.match(/^([^-]+)(?:-drop(\d+))?$/);
  var baseAlg = match[1];
  var drop = match[2] ? parseInt(match[2], 10) : 0;

  var hmacAlg = crypto.createHmac(baseAlg, key);
  var origDigest = hmacAlg.digest;

  if (drop === 0) {
    // Before 0.10, crypto returns binary-encoded strings. Remove when dropping
    // 0.8 support.
    hmacAlg.digest = function() {
      return forceBuffer(origDigest.call(this));
    };
  } else {
    var N = drop / 8; // bits to bytes
    hmacAlg.digest = function dropN() {
      var result = forceBuffer(origDigest.call(this));
      // Throw away the second half of the 512-bit result, leaving the first
      // 256-bits.
      var truncated = new Buffer(N);
      result.copy(truncated, 0, 0, N);
      zeroBuffer(result);
      return truncated;
    };
  }

  return hmacAlg;
}

function computeHmac(opts, iv, ciphertext, duration, createdAt) {
  var hmacAlg = hmacInit(opts.signatureAlgorithm, opts.signatureKey);

  hmacAlg.update(iv);
  hmacAlg.update(".");
  hmacAlg.update(ciphertext);
  hmacAlg.update(".");
  hmacAlg.update(createdAt.toString());
  hmacAlg.update(".");
  hmacAlg.update(duration.toString());

  return hmacAlg.digest();
};

function encode(opts, content, duration, createdAt) {
  // format will be:
  // iv.ciphertext.createdAt.duration.hmac

  setupKeys(opts);
  if (String(opts.cookieName).indexOf(COOKIE_NAME_SEP) !== -1) {
    throw new Error('cookieName cannot include "="');
  }

  duration = duration || 24*60*60*1000;
  createdAt = createdAt || new Date().getTime();

  // generate iv
  var iv = crypto.randomBytes(16);

  // encrypt with encryption key
  var plaintext = new Buffer(
    opts.cookieName + COOKIE_NAME_SEP + JSON.stringify(content),
    'utf8'
  );
  var cipher = crypto.createCipheriv(
    opts.encryptionAlgorithm,
    opts.encryptionKey,
    iv
  );

  var ciphertextStart = forceBuffer(cipher.update(plaintext));
  zeroBuffer(plaintext);
  var ciphertextEnd = forceBuffer(cipher.final());
  var ciphertext = Buffer.concat([ciphertextStart, ciphertextEnd]);
  zeroBuffer(ciphertextStart);
  zeroBuffer(ciphertextEnd);

  // hmac it
  var hmac = computeHmac(opts, iv, ciphertext, duration, createdAt);

  var result = [
    base64urlencode(iv),
    base64urlencode(ciphertext),
    createdAt.toString(NUMS_BASE),
    duration.toString(NUMS_BASE),
    base64urlencode(hmac)
  ].join('.');

  zeroBuffer(iv);
  zeroBuffer(ciphertext);
  zeroBuffer(hmac);

  return result;
}
exports.encode = encode;

function decode(opts, content) {
  NI.assert.isString(content);

  setupKeys(opts);

  // stop at any time if there's an issue
  var components = content.split(".");
  if (components.length !== 5) {
    return undefined;
  }


  var iv;
  var ciphertext;
  var hmac;

  try {
    iv = base64urldecode(components[0]);
    ciphertext = base64urldecode(components[1]);
    hmac = base64urldecode(components[4]);
  }
  catch (ignored) {
    cleanup();
    return undefined;
  }

  var createdAt = parseInt(components[2], NUMS_BASE);
  var duration = parseInt(components[3], NUMS_BASE);

  function cleanup() {
    if (iv) {
      zeroBuffer(iv);
    }

    if (ciphertext) {
      zeroBuffer(ciphertext);
    }

    if (hmac) {
      zeroBuffer(hmac);
    }

    if (expectedHmac) { // declared below
      zeroBuffer(expectedHmac);
    }
  }

  // make sure IV is right length
  if (iv.length !== 16) {
    cleanup();
    return undefined;
  }

  // check hmac
  var expectedHmac = computeHmac(opts, iv, ciphertext, duration, createdAt);

  if (!constantTimeEquals(hmac, expectedHmac)) {
    cleanup();
    return undefined;
  }

  // decrypt
  var cipher = crypto.createDecipheriv(
    opts.encryptionAlgorithm,
    opts.encryptionKey,
    iv
  );
  var plaintext = cipher.update(ciphertext, 'binary', 'utf8');
  plaintext += cipher.final('utf8');

  var cookieName = plaintext.substring(0, plaintext.indexOf(COOKIE_NAME_SEP));
  if (cookieName !== opts.cookieName) {
    cleanup();
    return undefined;
  }

  var result;
  try {
    result = {
      content: JSON.parse(
        plaintext.substring(plaintext.indexOf(COOKIE_NAME_SEP) + 1)
      ),
      createdAt: createdAt,
      duration: duration
    };
  }
  catch (ignored) {
    /* Do nothing */
  }

  cleanup();
  return result;
};
exports.decode = decode;
