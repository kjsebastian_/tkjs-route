var NI = require('tkjs-ni');
var azure = require('azure-storage');

var _requestsQueue = []

var defaultSecurity = 'blob'

var MulterAzureStorage = {

  constructor: function(opts) {
    var that = this;

    that.containerCreated = false
    that.containerError = false

    var missingParameters = []
    if (!opts.azureStorageConnectionString) {
      missingParameters.push("azureStorageConnectionString")
    }
    if (!opts.azureStorageAccessKey) {
      missingParameters.push("azureStorageAccessKey")
    }
    if (!opts.azureStorageAccount) {
      missingParameters.push("azureStorageAccount")
    }
    if (!opts.containerName) {
      missingParameters.push("containerName")
    }
    if (!opts.getStorageKey) {
      missingParameters.push("getStorageKey")
    }

    if (missingParameters.length > 0) {
      throw new Error('Missing required parameter' + (missingParameters.length > 1 ? 's' : '') + ' from the options of MulterAzureStorage: ' + missingParameters.join(', '))
    }

    that.getStorageKey = opts.getStorageKey;
    that.containerName = opts.containerName

    that.blobService = azure.createBlobService(
      opts.azureStorageAccount,
      opts.azureStorageAccessKey,
      opts.azureStorageConnectionString)

    var security = opts.containerSecurity || defaultSecurity

    that.blobService.createContainerIfNotExists(
      that.containerName,
      { publicAccessLevel : security },
      function(err, result, response) { // eslint-disable-line
        if (err) {
          that.containerError = true
          throw NI.Error('Cannot use container: %s.', NI.formatError(err));
        }

        that.containerCreated = true

        _requestsQueue.forEach(function(i) {
          that._removeFile(i.req, i.file, i.cb)
        })
        _requestsQueue = []
      })
  },

  _handleFile: function(req, file, cb) {
    var that = this;

    if (that.containerError) {
      cb(new Error('Cannot use container. Check if provided options are correct.'))
    }

    if (!that.containerCreated) {
      _requestsQueue.push({ req: req, file: file, cb: cb })
      return
    }

    that.getStorageKey(file,req).then(function(blob) {
      file.stream.pipe(that.blobService.createWriteStreamToBlockBlob(
        that.containerName, blob,
        function(err, azureBlob) { // eslint-disable-line
          if (err) {
            return cb(err)
          }

          that.blobService.getBlobProperties(
            that.containerName, blob,
            function(err, result, response) { // eslint-disable-line
              if (err) {
                return cb(err)
              }

              var url = that.blobService.getUrl(that.containerName, blob)
              cb(null, {
                container: result.container,
                blob: blob,
                blobType: result.blobType,
                size: result.contentLength,
                etag: result.etag,
                metadata: result.metadata,
                url: url
              })
            })
        }))
    }).catch(NI.promiseCatch);
  },

  _removeFile: function(req, file, cb) {
    var that = this;

    if (that.containerError) {
      cb(new Error('Cannot use container. Check if provided options are correct.'))
    }

    if (file.blobName) {
      that.blobService.deleteBlob(that.containerName, file.blobName, cb)
    } else {
      cb(null)
    }
  }
}

/**
 * @param {object}      [opts]
 * @param {string}      [opts.azureStorageConnectionString]
 * @param {string}      [opts.azureStorageAccessKey]
 * @param {string}      [opts.azureStorageAccount]
 * @param {string}      [opts.containerName]
 * @param {string}      [opts.containerSecurity]                'blob' or 'container', default: blob
 */
module.exports = function (opts) {
  var storage = NI.clone(MulterAzureStorage);
  storage.constructor(opts);
  return storage;
}
